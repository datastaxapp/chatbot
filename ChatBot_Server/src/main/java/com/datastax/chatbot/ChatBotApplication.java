package com.datastax.chatbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author bala2347
 *
 */
@SpringBootApplication
public class ChatBotApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(ChatBotApplication.class, args);
	}
}
