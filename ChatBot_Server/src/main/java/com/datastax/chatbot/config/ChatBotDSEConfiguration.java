/**
 * 
 */
package com.datastax.chatbot.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.datastax.driver.dse.DseCluster;
import com.datastax.driver.dse.DseSession;
import com.datastax.driver.dse.graph.GraphOptions;
import com.datastax.driver.dse.graph.GraphProtocol;

import ai.api.AIConfiguration;
import ai.api.AIDataService;

/**
 * @author bala2347
 *
 */
@Component
public class ChatBotDSEConfiguration {
	
	private static Logger logger = LoggerFactory.getLogger(ChatBotDSEConfiguration.class);
	
	@Value("${datastax.host}")
	private String dseHost;
	
	@Value("${datastax.graph}")
	private String graphName;
	
	@Value("${api.ai.token}")
	private String chatToken;
	
	/**
	 * @return
	 * @throws Exception
	 */
	@Bean
	public DseSession dseSession() throws Exception{
		DseCluster dseCluster = DseCluster.builder()
				.addContactPoint(dseHost)
				.withGraphOptions(new GraphOptions()
						.setGraphName(graphName)
						.setGraphSubProtocol(GraphProtocol.GRAPHSON_2_0))
				.build();
		logger.info("DseSession Created Successfully !!");
		return dseCluster.connect();
	}
	
	/**
	 * @return
	 * @throws Exception
	 */
	@Bean
	public AIDataService aiDataService() throws Exception{
		return new AIDataService(new AIConfiguration(chatToken));
	}

	/**
	 * @return the dseHost
	 */
	public String getDseHost() {
		return dseHost;
	}

	/**
	 * @param dseHost the dseHost to set
	 */
	public void setDseHost(String dseHost) {
		this.dseHost = dseHost;
	}

	/**
	 * @return the graphName
	 */
	public String getGraphName() {
		return graphName;
	}

	/**
	 * @param graphName the graphName to set
	 */
	public void setGraphName(String graphName) {
		this.graphName = graphName;
	}

	/**
	 * @return the chatToken
	 */
	public String getChatToken() {
		return chatToken;
	}

	/**
	 * @param chatToken the chatToken to set
	 */
	public void setChatToken(String chatToken) {
		this.chatToken = chatToken;
	}
}
