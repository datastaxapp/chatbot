/**
 * 
 */
package com.datastax.chatbot.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datastax.chatbot.model.Responses;
import com.datastax.chatbot.model.Users;
import com.datastax.chatbot.service.ChatBotService;

/**
 * @author bala2347
 *
 */
@CrossOrigin(methods= RequestMethod.POST)
@RestController
@RequestMapping("/chatbot")
public class ChatBotController {

	@Autowired
	ChatBotService chatBotService;

	/**
	 * @param users
	 * @return
	 */
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public Map<String, Object> create(@RequestBody Users users) {
		return chatBotService.initiateChat(users);
	}

	/**
	 * @param responses
	 * @return
	 */
	@RequestMapping(value="/response", method = RequestMethod.POST)
	public Responses getResponse(@RequestBody Responses responses){
		return chatBotService.getResponseMessage(responses);
	}
	
	/**
	 * @param users
	 */
	@RequestMapping(value="/clear", method = RequestMethod.POST)
	public Map<String, String> clearChatHistory(@RequestBody Users users) {
		return chatBotService.clearChatHistory(users);
	}
	
	/**
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="/history", method = RequestMethod.POST)
	public Map<String, Object> getChatHistory(@RequestBody Users users){
		return chatBotService.getChatHistory(users.getUserId());
	}
}
