/**
 * 
 */
package com.datastax.chatbot.model;

import java.io.Serializable;

/**
 * @author bala2347
 *
 */
public class Responses implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String userId;
	String message;
	String response;
	String messagePostedTime;
	String responseTime;
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}
	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}
	/**
	 * @return the messageTime
	 */
	public String getMessagePostedTime() {
		return messagePostedTime;
	}
	/**
	 * @param messageTime the messageTime to set
	 */
	public void setMessagePostedTime(String messagePostedTime) {
		this.messagePostedTime = messagePostedTime;
	}
	/**
	 * @return the responseTime
	 */
	public String getResponseTime() {
		return responseTime;
	}
	/**
	 * @param responseTime the responseTime to set
	 */
	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}
}
