/**
 * 
 */
package com.datastax.chatbot.service;

import java.util.Map;

import com.datastax.chatbot.model.Responses;
import com.datastax.chatbot.model.Users;

/**
 * @author bala2347
 *
 */
public interface ChatBotService {

	/**
	 * @param users
	 * @return
	 */
	public Map<String, Object> initiateChat(Users users);
	
	/**
	 * @param responses
	 * @return
	 */
	public Responses getResponseMessage(Responses responses);
	
	/**
	 * @param users
	 */
	public Map<String, String> clearChatHistory(Users users);
	
	/**
	 * @param userId
	 * @return
	 */
	public Map<String, Object> getChatHistory(String userId);
}
