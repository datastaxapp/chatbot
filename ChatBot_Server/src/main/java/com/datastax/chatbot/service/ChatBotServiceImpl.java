/**
 * 
 */
package com.datastax.chatbot.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.chatbot.model.Responses;
import com.datastax.chatbot.model.Users;
import com.datastax.driver.dse.DseSession;
import com.datastax.driver.dse.graph.SimpleGraphStatement;
import com.datastax.dse.graph.api.DseGraph;
import com.datastax.chatbot.util.ChatbotConstant;
import com.datastax.chatbot.util.DataStaxQuery;

import ai.api.AIDataService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author bala2347
 *
 */
@Service
public class ChatBotServiceImpl implements ChatBotService{
	
	private static Logger logger = LoggerFactory.getLogger(ChatBotServiceImpl.class);
	
	@Autowired
	private DseSession dseSession;
	
	@Autowired
	private AIDataService aiDataService;
	
	private DateFormat dseDateFormat = new SimpleDateFormat(ChatbotConstant.DATE_FORMAT);
	private DateFormat chatDateFormat = new SimpleDateFormat(ChatbotConstant.CHAT_DATE_FORMAT);

	/* (non-Javadoc)
	 * @see com.datastax.chatbot.service.ChatBotService#initiateChat(com.datastax.chatbot.model.Users)
	 */
	@Override
	public Map<String, Object> initiateChat(Users users) {
		Map<String,Object> userDetails = new HashMap<String, Object>();
		Boolean isExistingUser = Boolean.FALSE;
		if(users.getUserName() != null){
			GraphTraversalSource g = DseGraph.traversal(dseSession);
			
			GraphTraversal<Vertex, Vertex> graph = g.V().hasLabel(ChatbotConstant.USERS_LABEL).has(ChatbotConstant.USER_NAME, users.getUserName());
			Vertex vertex = null;
			if(graph.hasNext()){
				vertex = graph.next();
				isExistingUser = Boolean.TRUE;
				logger.info("User already exists !!");
				users.setName(vertex.properties(ChatbotConstant.NAME).next().value().toString());
				users.setUserId(vertex.properties(ChatbotConstant.USER_ID).next().value().toString());
				users.setCreatedOn(this.getFormattedDate(vertex.properties(ChatbotConstant.CREATED_ON).next().value().toString()));
				//userDetails.putAll(getChatHistory(users.getUserId()));
			} else{
				isExistingUser = Boolean.FALSE;
				logger.info("Creating new user : " + users.getUserName());
				users.setCreatedOn(this.getCurrentDateandTime());
				users.setName(users.getUserName());
				users.setUserId("USR" + System.currentTimeMillis());
				this.createNewUser(users);
			}
			userDetails.put(ChatbotConstant.USER, users);
			userDetails.put(ChatbotConstant.IS_EXISTING_USER, isExistingUser);
		}
		return userDetails;
	}

	/* (non-Javadoc)
	 * @see com.datastax.chatbot.service.ChatBotService#getResponseMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public Responses getResponseMessage(Responses responses) {
		if(responses.getUserId() != null && responses.getMessage() != null){
			responses.setMessagePostedTime(this.getCurrentDateandTime());
			
			try {
				AIRequest aiRequest = new AIRequest(responses.getMessage());
				AIResponse aiResponse = aiDataService.request(aiRequest);
				if (aiResponse.getStatus().getCode() == 200) {
					responses.setResponse(aiResponse.getResult().getFulfillment().getSpeech());
					responses.setResponseTime(this.getCurrentDateandTime());
					this.createMessageAndResponseVertex(responses);
				} else {
					logger.error(aiResponse.getStatus().getErrorDetails());
				}
			} catch (Exception ex) {
				logger.error("Exception : " + ex);
			}
		}
		return responses;
	}
	

	/* (non-Javadoc)
	 * @see com.datastax.chatbot.service.ChatBotService#getChatHistory(java.lang.String)
	 */
	@Override
	public Map<String, Object> getChatHistory(String userId) {
		ArrayList<Map<String, Object>> chatHistory = new ArrayList<Map<String, Object>>();
		Map<String, Object> chat = null;
		Map<String, Object> vertex = null;
		Vertex message = null;
		Vertex response = null;
		
		GraphTraversalSource g = DseGraph.traversal(dseSession);
		
		GraphTraversal<Vertex, Map<String, Object>> chatHistoryGraph = g.V().hasLabel(ChatbotConstant.USERS_LABEL).has(ChatbotConstant.USER_ID, userId)
				.out(ChatbotConstant.EDGE_OWNS)
				.out(ChatbotConstant.EDGE_POSTED)
				.order().by(ChatbotConstant.CREATED_ON).as(ChatbotConstant.USER_MESSAGE)
				.in(ChatbotConstant.EDGE_RESPONDED).as(ChatbotConstant.AI_RESPONSE)
				.select(ChatbotConstant.USER_MESSAGE, ChatbotConstant.AI_RESPONSE);
		
		while(chatHistoryGraph.hasNext()){
			chat = new HashMap<String, Object>();
			vertex = chatHistoryGraph.next();
			message = (Vertex) vertex.get(ChatbotConstant.USER_MESSAGE); 
			response = (Vertex) vertex.get(ChatbotConstant.AI_RESPONSE);
			
			chat.put("message", message.properties(ChatbotConstant.MESSAGE).next().value());
			chat.put("messagePostedTime", this.getFormattedDate(message.properties(ChatbotConstant.CREATED_ON).next().value().toString()));
			chat.put("response", response.properties(ChatbotConstant.RESPONSE_MSG).next().value());
			chat.put("responseTime", this.getFormattedDate(response.properties(ChatbotConstant.CREATED_ON).next().value().toString()));
			chatHistory.add(chat);
		}
		Map<String, Object> chatMap = new HashMap<String, Object>();
		chatMap.put(ChatbotConstant.CHAT_HISTORY, chatHistory);
		chatMap.put(ChatbotConstant.USER_ID, userId);
		return chatMap;
	}
	
	/**
	 * @param users
	 */
	private void createNewUser(Users users){
		dseSession.executeGraph(new SimpleGraphStatement(DataStaxQuery.USERS_VERTEX_QUERY)
						.set(ChatbotConstant.USER_ID, users.getUserId())
						.set(ChatbotConstant.USER_NAME, users.getUserName())
						.set(ChatbotConstant.NAME, users.getName())
						.set(ChatbotConstant.CREATED_ON, System.currentTimeMillis()));
		
		String chatroomID = "CHAT" + System.currentTimeMillis();		
		dseSession.executeGraph(new SimpleGraphStatement(DataStaxQuery.CHAT_ROOM_VERTEX_QUERY)
						.set(ChatbotConstant.CHAT_ROOM_ID, chatroomID)
						.set(ChatbotConstant.CREATED_ON, System.currentTimeMillis()));
		
		dseSession.executeGraph(new SimpleGraphStatement(DataStaxQuery.USERS_EDGE_QUERY)
						.set(ChatbotConstant.USER_ID, users.getUserId())
						.set(ChatbotConstant.CHAT_ROOM_ID, chatroomID));
		logger.info("New User Created !!");
	}
	
	/**
	 * @param responses
	 */
	private void createMessageAndResponseVertex(Responses responses){
		long currentTime = System.currentTimeMillis();
		String messageId = "MSG" + currentTime;
		String responseId = "RESP" + currentTime;
		
		dseSession.executeGraph( new SimpleGraphStatement(DataStaxQuery.MESSAGES_VERTEX_QUERY)
						.set(ChatbotConstant.MESSAGE_ID, messageId)
						.set(ChatbotConstant.MESSAGE, responses.getMessage())
						.set(ChatbotConstant.CREATED_ON, currentTime));
		
		dseSession.executeGraph( new SimpleGraphStatement(DataStaxQuery.MESSAGES_EDGE_QUERY)
                        .set(ChatbotConstant.MESSAGE_ID, messageId)
                        .set(ChatbotConstant.USER_ID, responses.getUserId()));
		
		dseSession.executeGraph(new SimpleGraphStatement(DataStaxQuery.RESPONSES_VERTEX_QUERY)
						.set(ChatbotConstant.RESPONSE_ID, responseId)
						.set(ChatbotConstant.RESPONSE_MSG, responses.getResponse())
						.set(ChatbotConstant.CREATED_ON, currentTime));
		
		dseSession.executeGraph(new SimpleGraphStatement(DataStaxQuery.RESPONSES_EDGE_QUERY)
						.set(ChatbotConstant.MESSAGE_ID, messageId)
						.set(ChatbotConstant.RESPONSE_ID, responseId));
	}

	/* (non-Javadoc)
	 * @see com.datastax.chatbot.service.ChatBotService#clearChatHistory(com.datastax.chatbot.model.Users)
	 */
	@Override
	public Map<String, String> clearChatHistory(Users users) {
		Map<String, String> status = new HashMap<String, String>();
		try{
			GraphTraversalSource g = DseGraph.traversal(dseSession);

			g.V().hasLabel(ChatbotConstant.USERS_LABEL).has(ChatbotConstant.USER_ID, users.getUserId())
					.out(ChatbotConstant.EDGE_OWNS).out(ChatbotConstant.EDGE_POSTED)
					.in(ChatbotConstant.EDGE_RESPONDED)
					.drop().iterate();
			
			g.V().hasLabel(ChatbotConstant.USERS_LABEL).has(ChatbotConstant.USER_ID, users.getUserId())
					.out(ChatbotConstant.EDGE_OWNS).out(ChatbotConstant.EDGE_POSTED)
					.drop().iterate();
			
			status.put(ChatbotConstant.STATUS, ChatbotConstant.SUCCESS);
			status.put(ChatbotConstant.MESSAGE, "Chat history cleard successfully.");
			logger.info("Chat history cleard !!");
		}catch (Exception e){
			status.put(ChatbotConstant.STATUS, ChatbotConstant.FAILED);
			status.put(ChatbotConstant.MESSAGE, e.toString());
			logger.error("Exception : " + e);
		}
		return status;
	}
	
	/**
	 * @param date
	 * @return
	 */
	private String getFormattedDate(String date){
		String formattedDate = null;
		try{
			chatDateFormat.setTimeZone(TimeZone.getDefault());
			dseDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			formattedDate = chatDateFormat.format(dseDateFormat.parse(date));
		}catch (Exception e){
			logger.error("Cannot parse date : " + e);
		}
		return formattedDate;
	}
	
	/**
	 * @return
	 */
	private String getCurrentDateandTime(){
		String currentDateandTime = null;
		try{
			chatDateFormat.setTimeZone(TimeZone.getDefault());
			currentDateandTime = chatDateFormat.format(new Date());
		}catch(Exception e){
			logger.error("Cannot parse date : " + e);
		}
		return currentDateandTime;
	}
}
