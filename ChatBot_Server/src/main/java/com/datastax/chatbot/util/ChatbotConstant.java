/**
 * 
 */
package com.datastax.chatbot.util;

/**
 * @author bala2347
 *
 */
public class ChatbotConstant {

	public static String USERS_LABEL = "Users";
	public static String CHAT_ROOMS_LABEL = "ChatRooms";
	public static String MESSAGES_LABEL = "Messages";
	public static String RESPONSES_LABEL = "Responses";
	
	public static String USER_NAME = "userName";
	public static String NAME = "name";
	public static String USER_ID = "userId";
	public static String CREATED_ON = "createdOn";
	public static String CHAT_ROOM_ID = "chatRoomId";
	public static String MESSAGE = "message";
	public static String MESSAGE_ID = "messageId";
	public static String RESPONSE_MSG = "responseMsg";
	public static String RESPONSE_ID = "responseId";
	
	public static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static String CHAT_DATE_FORMAT = "MM-dd-yyyy hh:mm:ss a";
	
	public static String IS_EXISTING_USER = "isExistingUser";
	public static String USER = "user";
	public static String CHAT_HISTORY = "chatHistory";
	
	public static String EDGE_OWNS = "owns";
	public static String EDGE_POSTED = "posted";
	public static String EDGE_RESPONDED = "responded";
	
	public static String USER_MESSAGE = "USER_MESSAGE";
	public static String AI_RESPONSE = "AI_RESPONSE";
	
	public static String STATUS = "status";
	public static String SUCCESS = "success";
	public static String FAILED = "failed";
}
