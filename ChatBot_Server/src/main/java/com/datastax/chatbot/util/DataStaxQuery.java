/**
 * 
 */
package com.datastax.chatbot.util;

/**
 * @author bala2347
 *
 */
public class DataStaxQuery {

	public static String USERS_VERTEX_QUERY = "g.addV(label, 'Users', 'userId', userId, 'userName', userName, 'name', name, 'createdOn', createdOn)";
	
	public static String CHAT_ROOM_VERTEX_QUERY = "g.addV(label, 'ChatRooms', 'chatRoomId', chatRoomId, 'createdOn', createdOn)";
	
	public static String MESSAGES_VERTEX_QUERY = "g.addV(label, 'Messages', 'messageId', messageId, 'message', message, 'createdOn', createdOn)";
	
	public static String RESPONSES_VERTEX_QUERY = "g.addV(label, 'Responses', 'responseId', responseId, 'responseMsg', responseMsg, 'createdOn', createdOn)";
	
	public static String USERS_EDGE_QUERY = "user = g.V().has('Users','userId', userId).next();"
											+ "chatroom = g.V().has('ChatRooms','chatRoomId', chatRoomId).next();"
											+ "user.addEdge('owns',chatroom);";
	
	public static String MESSAGES_EDGE_QUERY = "message = g.V().has('Messages','messageId', messageId).next();"
											+ "chatroom = g.V().has('Users','userId', userId).out('owns').next();"
											+ "chatroom.addEdge('posted',message);";
	
	public static String RESPONSES_EDGE_QUERY = "message = g.V().has('Messages','messageId', messageId).next();"
											+ "response = g.V().has('Responses','responseId', responseId).next();"
											+ "response.addEdge('responded',message);";
}
