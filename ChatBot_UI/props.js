'use strict';

//ip for Host 
const host='172.24.35.173';
const host_port = '8000';

//ip for receiving Messaging Service 
const msg_service_host ='http://172.24.35.102:8182';


module.exports = {
 host : host,
 host_port : host_port,
 msg_service_host : msg_service_host

};