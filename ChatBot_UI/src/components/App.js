
import React from 'react';
import ChatApp from './ChatApp';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import axios from "axios";
import Scrollbar from 'react-smooth-scrollbar';
import Response from './Response';
require('../styles/App.css');
require('../styles/Login.css');
require('../styles/ChatApp.css');

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { username: '', userId: '', chatClose: true };

    // Bind 'this' to event handlers. React ES6 does not do this by default
    this.usernameChangeHandler = this.usernameChangeHandler.bind(this);
    this.usernameSubmitHandler = this.usernameSubmitHandler.bind(this);
    this.closeChatHandler = this.closeChatHandler.bind(this);

  }

  closeChatHandler() {
    this.setState({ chatClose: !this.state.chatClose });
  }

  usernameChangeHandler(event) {
    this.setState({ username: event.target.value });
  }

  usernameSubmitHandler(event) {
    event.preventDefault();
    //here for an take an request to chatHistory for an existing user
    this.setState({ submitted: true, username: this.state.username });
   
  }
  render() {
    if (this.state.chatClose) {
      return (
        <div className="container-chat view hm-zoom" onClick={this.closeChatHandler} >
          <img src={require('../images/chat-icon.png')} className="rounded chat-image" />
          <span className="init-chat-text">ASK TESSA</span>
        </div>
      );
    }
    if (this.state.submitted) {
      
      // Form was submitted, now show the main App
      return (
        <ChatApp username={this.state.username}  handlerFromParant={this.closeChatHandler} />
      );
    }
    // Initial page load, show a simple login form
    return (
      <div >
        <div className="container">
          <header className="clearfix" >
            <h4> <a href="#"> <i className="fa fa-comments"></i></a>  <span className="online">NEED HELP? ASK TESSA</span>
              <a href="#" onClick={this.closeChatHandler} className="chat-close" data-toggle="tooltip" data-placement="top" title="Close" ><i className="fa fa-close"></i>      </a>   </h4>
          </header>
          <div className="main-body" >
            <form onSubmit={this.usernameSubmitHandler} className="username-container">

              <div className="initial">
                {/* <p>To Initiate </p> */}
                <label >To Initiate</label>
                <span>&nbsp; </span>

                <input
                  type="text" className="start"
                  onChange={this.usernameChangeHandler}
                  placeholder="Enter a username..."
                  required />
                <span>&nbsp; </span>
                <input type="submit"  data-toggle="tooltip" data-placement="top" title="Start" className="btn btn-start btn-sm" value=">>" />
              </div>

            </form>
          </div>

        </div>
      </div>
    );
  }
}

App.defaultProps = {
};

export default App;
