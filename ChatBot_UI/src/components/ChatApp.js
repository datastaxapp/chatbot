
import React from 'react';
import Messages from './Messages';
import ChatInput from './ChatInput';
import axios from 'axios';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import 'react-select/dist/react-select.css';
require('../styles/ChatApp.css');
require('font-awesome/css/font-awesome.css');
import Websocket from 'react-websocket';

import SockJS from 'sockjs-client';
import webstomp from 'webstomp-client';


var stompClient = null;
let props = require('../../props');

let service_url = props.msg_service_host+'/chatbot/';

class ChatApp extends React.Component {
  socket = {};
  constructor(props) {
    super(props);
    this.state = { messages: [], chatHistory: {}, maxbool: true, userId: '', isExistingUser: false,suggestionMsg:'' };
    this.sendHandler = this.sendHandler.bind(this);
    this.maximizeWindow = this.maximizeWindow.bind(this);
    this.clearHistory = this.clearHistory.bind(this);
    this.chatClose = this.chatClose.bind(this);
    this.loadHistory = this.loadHistory.bind(this);
  }


//method for load history
  loadHistory() {
    var history = axios({
      method: 'post',
      url: service_url+'history',
      data: {
        userId: this.state.userId
      }
    })
      .then(function (response) {
        return JSON.stringify(response.data);
      })
      .catch(function (error) {
        console.log('failed');
      });
    var historyAssign = history.then(value => {
      const chatHistory = value;
      this.setState({ chatHistory });
    }, 1000);

    setTimeout(function () {
      this.updateMessage();

    }.bind(this), 1000);
  }

//method for clear history

  clearHistory() {
    confirmAlert({
     message: 'Click \'Yes\' to confirm clear chat history',
      confirmLabel: 'No',
      cancelLabel: 'Yes',
      onConfirm: () => {
      },
      onCancel: () => {
        var resForClear =  axios({
          method: 'post',
          url: service_url+'clear',
          data: {
            userId: this.state.userId
          }
        })
          .then(function (response, status) {
            status = response.data.status;
            return (response.data);
          })
          .catch(function (error) {
          });
       

        resForClear.then(value => {
          if (value.status == 'success') {
            this.setState({ messages: [] });
          }
        }, 1000)

      },      // Action after Cancel
    });
  }

//method for response in state

  updateMessage() {
    const chatHObj = JSON.parse(this.state.chatHistory);
    Object.seal(chatHObj);
    var msgArray = [];
    const cHistory = chatHObj.chatHistory;

    cHistory.map((history, i) => {
     
      Object.seal(history);
      //const history1 = JSON.parse(history);
      msgArray = this.state.messages;
      msgArray.push({
        'message': history.message, 'response': history.response,
        'messagePostedTime': history.messagePostedTime, 'responseTime': history.responseTime
      });
    });

    this.setState({ messages: msgArray });
   
  }

  componentDidMount() {

    var userVal = axios({
      method: 'post',
      url: service_url+'create',
      data: {
        userName: this.props.username
      }
    })
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log('failed');
      });

     var historyAssign = userVal.then(value => {
      var user = value.user;
      this.setState({ userId: user.userId });
    }, 1000);
 
   
   
  }


  //handler for input message

  sendHandler(message) {
    const messageObject = {
      username: this.props.username, message
    };
    // Emit the message to the server
    //this.socket.emit('client:message', messageObject);
    messageObject.fromMe = true;
    this.addMessage(messageObject);
  }

//method for getting response from service 
  addMessage(message) {

    // Append the message to the component state
    const messages = this.state.messages;
    messages.push({
      message: message.message, response: '',
      messagePostedTime: '', responseTime: ''
    });
    this.setState({ messages });
    //getting responses
    var responseVal = axios({
      method: 'post',
      url:service_url+'/response',
      data: { userId: this.state.userId, message: message.message }
    }).then(function (response) {
      return response.data;
    })
      .catch(function (error) {
        console.log('failed');
      });

    var responseAssign = responseVal.then(value => {
      var lastIndex = messages.length - 1;
      var obj = {
        message: message.message, response: value.response,
        messagePostedTime: value.messagePostedTime, responseTime: value.responseTime
      };
      messages.splice(lastIndex, 1, obj);
      this.setState({ messages });
    });

    this.setState({ suggestionMsg:'' });
  }

  maximizeWindow() {
    this.setState({ maxbool: !this.state.maxbool });
  }

  chatClose() {
    this.props.handlerFromParant();
  }
  render() {
    if (this.state.maxbool) {
      return (
        <div className="container">
          <header className="clearfix" >
            <h4>   <a href="#"> <i className="fa fa-comments"></i></a>  <span className="online">NEED HELP? ASK TESSA</span>
              <a href="#" onClick={this.chatClose} className="chat-close" data-toggle="tooltip" data-placement="top" title="Close" ><i className="fa fa-close"></i>      </a>
              <a href="#" className="chat-setting" data-toggle="tooltip" data-placement="top" title="Settings" >   <i className="fa fa-cog"></i> </a>
              <a href="#" onClick={this.maximizeWindow} className="chat-min" data-toggle="tooltip" data-placement="top" title="Minimize" ><i className='fa fa-angle-double-down'></i>  </a>
            </h4>
          </header>
          <div className="main-body" >
            <span className="history">
              <a href="#" onClick={this.loadHistory}    >History </a> | <a href="#" onClick={this.clearHistory} >Clear</a>
            </span>
            <hr className='style1'></hr>
            <Messages messages={this.state.messages}  username={this.props.username} suggestionHandler={this.getSuggestion}   />
            <ChatInput onSend={this.sendHandler} suggestionMsg={this.state.suggestionMsg} />
          </div>
        </div>

      );
    } else {
      return (
        <div className="container container-min">
          <header className="clearfix" onClick={this.maximizeWindow}>
            <h4> <a href="#"    > <i className="fa fa-comments"></i></a>  <span className="online">NEED HELP? ASK TESSA</span>
              <a href="#" onClick={this.chatClose} className="chat-close" data-toggle="tooltip" data-placement="top" title="Close"><i className="fa fa-close"></i>      </a>
              <a href="#" onClick={this.maximizeWindow} className="chat-min" data-toggle="tooltip" data-placement="top" title="Maximize" ><i className='fa fa-angle-double-up'></i>  </a>     </h4>

          </header>
        </div>
      )
    }
  }
}
ChatApp.defaultProps = {
  username: 'Anonymous'
};

export default ChatApp;
