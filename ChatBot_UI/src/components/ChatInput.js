
import React from 'react';
require('font-awesome/css/font-awesome.css');

class ChatInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { chatInput: '' };

    // React ES6 does not bind 'this' to event handlers by default
    this.submitHandler = this.submitHandler.bind(this);
    this.textChangeHandler = this.textChangeHandler.bind(this);
  }

  componentWillReceiveProps(nextProps){
   
      this.setState({ chatInput: nextProps.suggestionMsg });
     
  }

  submitHandler(event) {
    // Stop the form from refreshing the page on submit
    event.preventDefault();

    // Clear the input box
    this.setState({ chatInput: '' });

    // Call the onSend callback with the chatInput message
    this.props.onSend(this.state.chatInput);
  }

  textChangeHandler(event) {
    this.setState({ chatInput: event.target.value });
  }

  render() {
    return (
      <form className="chat-input" onSubmit={this.submitHandler}>
        <input type="text"
          onChange={this.textChangeHandler}
          value={this.state.chatInput}
          placeholder="Ask Something..."
          required >
        </input>
        <button>  <i className="fa fa-paper-plane"></i></button>
      </form>
    );
  }
}

ChatInput.defaultProps = {
};

export default ChatInput;
