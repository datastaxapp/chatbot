import React from 'react';

class Message extends React.Component {

  render() {
    // Was the message sent by the current user. If so, add a css class
    const fromMe = this.props.fromMe ? 'from-me' : '';

    return (

      <div className={`message ${fromMe} `}>
        <div className='username'>
          {this.props.username}
        </div>

        <div className='message-body'>
          {this.props.message} <br></br>
          <p className="timestamp-message"> {this.props.messagePostedTime}</p>

        </div>
        <div className="image-cropper">
          <img src={require('../images/user.png')} className="rounded" />
        </div>
      </div>
    );
  }
}

Message.defaultProps = {
  message: '',

  username: '',
  fromMe: false
};

export default Message;
