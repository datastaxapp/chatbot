import React from 'react';
import Message from './Message';
import Response from './Response';


class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.state = { messages: [] };

    this.suggestionHandler = this.suggestionHandler.bind(this);

  }

 
 suggestionHandler(event){

  var suggestionMsg = event.target.value;
  this.props.suggestionHandler(suggestionMsg);

 }


  componentDidUpdate() {
    // There is a new message in the state, scroll to bottom of list
    const objDiv = document.getElementById('messageList');
    objDiv.scrollTop = objDiv.scrollHeight;
  }

  render() {

   
    // Loop through all the messages in the state and create a Message component
    const messages = this.props.messages.map((msg, i) => {

      return (
        <div key={i}>
          <Message
            username={this.props.username}
            msgId={i}
            message={msg.message}
            messagePostedTime={msg.messagePostedTime}
            fromMe={true} />
          <Response
            username={this.props.username}
            msgId={i}
            response={msg.response}
            responseTime={msg.responseTime}
            fromMe={true} />
        </div>
      );
    });

    return (
      <div className='messages' id='messageList'>

        <div className='greeting'>

          <div className="image-cropper-left">
            <img src={require('../images/AI_user.png')} className="rounded" />
          </div>
          <div className='response-body '>
            {'Hi! I am Tessa. Welcomes you. Please ask your queries.'}
            <br></br>
            {/* <p className="timestamp"> {this.props.responseTime}</p> */}
          </div >
          
        </div>


        {messages}

      </div>
    );
  }
}

Messages.defaultProps = {
  messages: []
};

export default Messages;
