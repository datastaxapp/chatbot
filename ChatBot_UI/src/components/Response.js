import React from 'react';

class Response extends React.Component {

    render() {
        const fromMe = this.props.fromMe ? 'from-me' : '';

        if (this.props.response == '') {
            return (
                <div className="response-small">
                <div className="image-cropper-left">
                    <img src={require('../images/AI_user.png')} className="rounded" />
                </div>
                <div className='response-body-small'>
                    {'...'}<br></br>

                </div>
            </div>

            );
        }

        return (

          <div>  
            <div className={`response ${fromMe}`}>

                <div className="image-cropper-left">
                    <img src={require('../images/AI_user.png')} className="rounded" />
                </div>
                <div className='response-body '>
                    {this.props.response}<br></br>
                    <p className="timestamp"> {this.props.responseTime}</p>
                </div>
            </div>
          </div>
        );
    }
}

Response.defaultProps = {
    message: '',
    username: '',
    fromMe: true
};

export default Response;